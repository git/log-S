/*
 * Copyright (C) 2005 Junio C Hamano
 * Copyright (C) 2010 Google Inc.
 */
#include "cache.h"
#include "diff.h"
#include "diffcore.h"
#include "xdiff-interface.h"

struct diffgrep_cb {
	regex_t *regexp;
	int hit;
};

static void diffgrep_consume(void *priv, char *line, unsigned long len)
{
	struct diffgrep_cb *data = priv;
	regmatch_t regmatch;
	int hold;

	if (line[0] != '+' && line[0] != '-')
		return;
	if (data->hit)
		/*
		 * NEEDSWORK: we should have a way to terminate the
		 * caller early.
		 */
		return;
	/* Yuck -- line ought to be "const char *"! */
	hold = line[len];
	line[len] = '\0';
	data->hit = !regexec(data->regexp, line + 1, 1, &regmatch, 0);
	line[len] = hold;
}

static void fill_one(struct diff_filespec *one,
		     mmfile_t *mf, struct userdiff_driver **textconv)
{
	if (DIFF_FILE_VALID(one)) {
		*textconv = get_textconv(one);
		mf->size = fill_textconv(*textconv, one, &mf->ptr);
	} else {
		memset(mf, 0, sizeof(*mf));
	}
}

static int diff_grep(struct diff_filepair *p, regex_t *regexp, struct diff_options *o)
{
	regmatch_t regmatch;
	struct userdiff_driver *textconv_one = NULL;
	struct userdiff_driver *textconv_two = NULL;
	mmfile_t mf1, mf2;
	int hit;

	if (diff_unmodified_pair(p))
		return 0;

	fill_one(p->one, &mf1, &textconv_one);
	fill_one(p->two, &mf2, &textconv_two);

	if (!mf1.ptr) {
		if (!mf2.ptr)
			return 0; /* ignore unmerged */
		/* created "two" -- does it have what we are looking for? */
		hit = !regexec(regexp, p->two->data, 1, &regmatch, 0);
	} else if (!mf2.ptr) {
		/* removed "one" -- did it have what we are looking for? */
		hit = !regexec(regexp, p->one->data, 1, &regmatch, 0);
	} else {
		/*
		 * We have both sides; need to run textual diff and see if
		 * the pattern appears on added/deleted lines.
		 */
		struct diffgrep_cb ecbdata;
		xpparam_t xpp;
		xdemitconf_t xecfg;

		memset(&xpp, 0, sizeof(xpp));
		memset(&xecfg, 0, sizeof(xecfg));
		ecbdata.regexp = regexp;
		ecbdata.hit = 0;
		xecfg.ctxlen = o->context;
		xecfg.interhunkctxlen = o->interhunkcontext;
		xdi_diff_outf(&mf1, &mf2, diffgrep_consume, &ecbdata,
			      &xpp, &xecfg);
		hit = ecbdata.hit;
	}
	if (textconv_one)
		free(mf1.ptr);
	if (textconv_two)
		free(mf2.ptr);
	return hit;
}

static void diffcore_pickaxe_grep(struct diff_options *o)
{
	struct diff_queue_struct *q = &diff_queued_diff;
	int i, has_changes, err;
	regex_t regex;
	struct diff_queue_struct outq;
	outq.queue = NULL;
	outq.nr = outq.alloc = 0;

	err = regcomp(&regex, o->pickaxe, REG_EXTENDED | REG_NEWLINE);
	if (err) {
		char errbuf[1024];
		regerror(err, &regex, errbuf, 1024);
		regfree(&regex);
		die("invalid log-grep regex: %s", errbuf);
	}

	if (o->pickaxe_opts & DIFF_PICKAXE_ALL) {
		/* Showing the whole changeset if needle exists */
		for (i = has_changes = 0; !has_changes && i < q->nr; i++) {
			struct diff_filepair *p = q->queue[i];
			if (diff_grep(p, &regex, o))
				has_changes++;
		}
		if (has_changes)
			return; /* do not munge the queue */

		/*
		 * Otherwise we will clear the whole queue by copying
		 * the empty outq at the end of this function, but
		 * first clear the current entries in the queue.
		 */
		for (i = 0; i < q->nr; i++)
			diff_free_filepair(q->queue[i]);
	} else {
		/* Showing only the filepairs that has the needle */
		for (i = 0; i < q->nr; i++) {
			struct diff_filepair *p = q->queue[i];
			if (diff_grep(p, &regex, o))
				diff_q(&outq, p);
			else
				diff_free_filepair(p);
		}
	}

	regfree(&regex);

	free(q->queue);
	*q = outq;
	return;
}

struct needle {
    unsigned skip[256];
    unsigned long len;
    int use_regex;
    regex_t regex;
    struct hashtbl {
        unsigned long mask;
        unsigned long entry_count;
        struct hashentry {
            char sha1[20];
            int value;
        } *free_entries;
        int free_entry_count;
        struct hashentry **table;
    } hashtbl;
    unsigned char text[];
};

static struct hashentry *hash_lookup(struct hashtbl *h, unsigned char sha1[20], int *found)
{
    struct hashentry *e;
    unsigned long hash = *(unsigned long *)sha1;
    while (1) {
        hash &= h->mask;
        struct hashentry *e = h->table[hash];
        if (!e)
            break;
        if (!memcmp(sha1, e->sha1, 20)) {
            *found = 1;
            return e;
        }
        hash++;
    }

    *found = 0;
    if (!h->free_entry_count) {
        h->free_entries = xmalloc(100 * sizeof(*h->free_entries));
        h->free_entry_count = 100;
    }
    h->free_entry_count--;
    e = h->free_entries++;
    h->table[hash] = e;
    memcpy(e->sha1, sha1, 20);
    h->entry_count++;
    if (h->mask <= (h->entry_count << 1)) {
        struct hashentry **oldtable = h->table;
        unsigned long oldmask = h->mask;
        h->mask = h->mask * 4 + 3;
        h->table = xcalloc(h->mask + 1, sizeof(*h->table));
        long i;
        for (i = 0; i <= oldmask; i++) {
            struct hashentry *moved = oldtable[i];
            if (!moved)
                continue;
            hash = *(unsigned long *)(moved->sha1);
            while (1) {
                hash &= h->mask;
                if (!h->table[hash])
                    break;
                hash++;
            }
            h->table[hash] = moved;
        }
        free(oldtable);
    }
    return e;
}

static void bmh_init(struct needle *needle)
{
    int i;
    for (i = 0; i < 256; i++)
        needle->skip[i] = needle->len;

    // Last character in needle can not be a mismatch in last position */
    for (i = 0; i < needle->len - 1; i++)
        needle->skip[needle->text[i]] = needle->len - i - 1;
}


static unsigned bmh(const unsigned char *haystack, unsigned long hlen,
		    const struct needle *needle)
{
    int i;

    if (hlen < needle->len)
        return 0;

    const unsigned char *end = haystack + hlen - needle->len;
    unsigned cnt = 0;
    while (haystack <= end) {
        while (haystack[needle->len - 1] != needle->text[needle->len - 1]) {
            haystack += needle->skip[haystack[needle->len - 1]];
            if (haystack > end)
                return cnt;
        }
        for (i = 0; i < needle->len - 1; i++)
            if (haystack[i] != needle->text[i])
                goto mismatch;
        cnt++;
        haystack += needle->len;
        continue;
    mismatch:
        haystack += needle->skip[haystack[needle->len - 1]];
    }

    return cnt;
}


static unsigned int contains(struct diff_filespec *one,
			     struct needle *needle)
{
	unsigned int cnt;
	unsigned long sz;
	const char *data;
	if (!needle->len)
		return 0;

        struct hashentry *store_ptr;
	if (one->sha1_valid) {
                int found;
                store_ptr = hash_lookup(&needle->hashtbl, one->sha1, &found);
                //printf("%s %d\n", sha1_to_hex(one->sha1), found);
		if (found)
                        return store_ptr->value;
	}

	if (diff_populate_filespec(one, 0))
		return 0;

	sz = one->size;
	data = one->data;
	cnt = 0;

	if (needle->use_regex) {
		regmatch_t regmatch;
		int flags = 0;

		assert(data[sz] == '\0');
		while (*data && !regexec(&needle->regex, data, 1, &regmatch, flags)) {
			flags |= REG_NOTBOL;
			data += regmatch.rm_eo;
			if (*data && regmatch.rm_so == regmatch.rm_eo)
				data++;
			cnt++;
		}

	} else
            cnt = bmh(data, sz, needle);
	if (one->sha1_valid)
                store_ptr->value = cnt;
	diff_free_filespec_data(one);
	return cnt;
}

static void diffcore_pickaxe_count(struct diff_options *o)
{
	const char *needle_txt = o->pickaxe;
	int opts = o->pickaxe_opts;
	struct diff_queue_struct *q = &diff_queued_diff;
	unsigned long len = strlen(needle_txt);
	int i, has_changes;
	struct diff_queue_struct outq;
	DIFF_QUEUE_CLEAR(&outq);
        static struct needle *needle = NULL;

        if (needle)
            goto init_done;
	if (opts & DIFF_PICKAXE_REGEX) {
		needle = xcalloc(1, sizeof(*needle));
		needle->use_regex = 1;
		needle->len = len;
		int err;
		err = regcomp(&needle->regex, needle_txt, REG_EXTENDED | REG_NEWLINE);
		if (err) {
			/* The POSIX.2 people are surely sick */
			char errbuf[1024];
			regerror(err, &needle->regex, errbuf, 1024);
			regfree(&needle->regex);
			die("invalid pickaxe regex: %s", errbuf);
		}
	}
        else {
		needle = xcalloc(1, sizeof(*needle) + len);
		memcpy(needle->text, needle_txt, len);
		needle->len = len;
		bmh_init(needle);
	}
        needle->hashtbl.table = calloc(1, sizeof(*needle->hashtbl.table));
 init_done:

	if (opts & DIFF_PICKAXE_ALL) {
		/* Showing the whole changeset if needle exists */
		for (i = has_changes = 0; !has_changes && i < q->nr; i++) {
			struct diff_filepair *p = q->queue[i];
			if (!DIFF_FILE_VALID(p->one)) {
				if (!DIFF_FILE_VALID(p->two))
					continue; /* ignore unmerged */
				/* created */
				if (contains(p->two, needle))
					has_changes++;
			}
			else if (!DIFF_FILE_VALID(p->two)) {
				if (contains(p->one, needle))
					has_changes++;
			}
			else if (!diff_unmodified_pair(p) &&
				 contains(p->one, needle) !=
				 contains(p->two, needle))
				has_changes++;
		}
		if (has_changes)
			return; /* not munge the queue */

		/* otherwise we will clear the whole queue
		 * by copying the empty outq at the end of this
		 * function, but first clear the current entries
		 * in the queue.
		 */
		for (i = 0; i < q->nr; i++)
			diff_free_filepair(q->queue[i]);
	}
	else
		/* Showing only the filepairs that has the needle */
		for (i = 0; i < q->nr; i++) {
			struct diff_filepair *p = q->queue[i];
			has_changes = 0;
			if (!DIFF_FILE_VALID(p->one)) {
				if (!DIFF_FILE_VALID(p->two))
					; /* ignore unmerged */
				/* created */
				else if (contains(p->two, needle))
					has_changes = 1;
			}
			else if (!DIFF_FILE_VALID(p->two)) {
				if (contains(p->one, needle))
					has_changes = 1;
			}
			else if (!diff_unmodified_pair(p) &&
				 contains(p->one, needle) !=
				 contains(p->two, needle))
				has_changes = 1;

			if (has_changes)
				diff_q(&outq, p);
			else
				diff_free_filepair(p);
		}
#if 0

	if (opts & DIFF_PICKAXE_REGEX)
		regfree(&needle->regex);
        free(needle);
#endif

	free(q->queue);
	*q = outq;
	return;
}

void diffcore_pickaxe(struct diff_options *o)
{
	/* Might want to warn when both S and G are on; I don't care... */
	if (o->pickaxe_opts & DIFF_PICKAXE_KIND_G)
		diffcore_pickaxe_grep(o);
	else
		diffcore_pickaxe_count(o);
}
